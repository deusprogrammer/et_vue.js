export default {
  addToCart: function(shopId, product, cookies) {
    var shopIndex = "shop" + shopId;
    var cartIndex = shopIndex + 'cart'
    var productIndex = "product" + product.id;

    var cart = this.getCart(shopId, cookies);

    if (cart == undefined) {
      cart = {};
    }

    if (cart[productIndex] != undefined) {
      return
    }

    cart[productIndex] = product;
    
    this.storeCart(shopId, cart, cookies);
  },
  incrementItemInCart: function(shopId, productId, cookies) {
    var cart = this.getCart(shopId, cookies);
    var productIndex = "product" + productId;
    cart[productIndex].quantity = cart[productIndex].quantity + 1;

    this.storeCart(shopId, cart, cookies);
  },
  decrementItemInCart: function(shopId, productId, cookies) {
    var cart = getCart(shopId, cookies);
    var productIndex = "product" + productId;
    cart[productIndex].quantity = cart[productIndex].quantity - 1;

    this.storeCart(shopId, cart, cookies);
  },
  getCart: function(shopId, cookies) {
    var shopIndex = "shop" + shopId;
    var cartIndex = shopIndex + 'cart'

    var cartString = cookies.get(cartIndex);

    if (cartString == undefined) {
      return undefined;
    }

    return JSON.parse(cartString);
  },
  storeCart: function(shopId, cart, cookies) {
    var shopIndex = "shop" + shopId;
    var cartIndex = shopIndex + 'cart'

    var cartString = JSON.stringify(cart);

    cookies.set(cartIndex, cartString);
  },
  clearCart: function(shopId, cookies) {
    var shopIndex = "shop" + shopId;
    var cartIndex = shopIndex + 'cart'

    cookies.set(cartIndex, JSON.stringify({}));
  }
}