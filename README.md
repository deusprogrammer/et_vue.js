# TODO

* Admin
	* Login dealer
	* Products
		* Add image to create product
* Shop
	* Login customer (OPTIONAL)
	* Create customer (OPTIONAL)
	* Profile page (OPTIONAL)
		* View orders (OPTIONAL)
		* Order detail (OPTIONAL)
	* Products
		* Retrieve image on get product/products
* Backend
	* Order
		* Item/Quantity subfield
			* Capture price of each item when order is created
		* Shipping service field
		* Add shipping status field
		* Tracking number field
		* Date shipping field
		* Expected delivery field
		* Note fields
		* Attributes field
		* Change createdOn string fields to actual date fields
* Future
	* Ability for a store to setup what shipping services it offers
* Parts not functional
	* Shipping status
	* Payment
	* Dealer login
	* Customer creation
	* Customer login
	* Categories
	* Searches
	* Product update